const { execSync } = require('child_process')
const command = '/usr/local/bin/node --expose-http2 -- /Users/sebdeckers/code/pop/node_modules/.bin/http2server restart'

setInterval(() => {
  console.log(`Restarting at ${new Date()}`)
  execSync(command)
}, 180000)
